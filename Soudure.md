Tuto Fr

 * [OpenCircuit](https://opencircuit.fr/blog/solderen-pro-tips-perfecte-soldeerverbindingen)
 * [WikiHow](https://fr.wikihow.com/souder-des-composants-%C3%A9lectroniques)
 * [Sonelec](https://www.sonelec-musique.com/electronique_bases_tutoriel_soudure.html)
 * [IFixit](https://fr.ifixit.com/Tutoriel/Kit+d'introduction+%C3%A0+l'%C3%A9lectronique/6190)
 * [InterFace-Z](https://www.interface-z.com/conseil/soudure.php#principes)


Tuto En

 * [MakerSpace](https://www.makerspaces.com/how-to-solder/)
 * [WikiHow](https://www.wikihow.com/Solder-Electronics)
 * [IFixIt](https://www.ifixit.com/News/6864/how-to-solder)
 * [ScienceBuddies](https://www.sciencebuddies.org/science-fair-projects/references/how-to-solder)
 